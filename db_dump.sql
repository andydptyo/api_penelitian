/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 5.1.30-community : Database - db_pendidikan_nondm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_pendidikan_nondm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_pendidikan_nondm`;

/*Table structure for table `peneliti` */

DROP TABLE IF EXISTS `peneliti`;

CREATE TABLE `peneliti` (
  `id` bigint(6) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) NOT NULL,
  `nama_peneliti` varchar(255) NOT NULL,
  `telepon` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pob` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `instansi` varchar(255) DEFAULT NULL,
  `fakultas` varchar(255) DEFAULT NULL,
  `jurusan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
