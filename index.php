<?php
require 'flight/Flight.php';

//get database
try {
    $mydbhost = 'localhost';
    $mydbuser = 'root';
    $mydbpass = '';
    $mydbname = 'db_pendidikan_nondm';

    $mydbmethod = 'mysql:host=' . $mydbhost . ';dbname=' . $mydbname;
    $mypdo = new PDO($mydbmethod, $mydbuser, $mydbpass);
    $mypdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // $myorm = new NotORM($mypdo);
} catch (PDOException $e) {
    echo "err: " . $e->getMessage();
    // $app->error($e->getMessage());

}

Flight::route('/', function () {
    echo 'hello world!';
});

// Get all data
Flight::route('GET /peneliti', function () use ($mypdo) {
    $query = "SELECT id, nik, nama_peneliti, telepon, email, instansi FROM peneliti";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $output = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $output = array(
            'status' => 204,
            'message' => 'No Data Available',
        );
    }

    Flight::json($output);
});

// Get data by id
Flight::route('GET /peneliti/@id', function ($id) use ($mypdo) {
    $query = "SELECT * FROM peneliti WHERE id = $id";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $output = array(
        'status' => 200,
        'message' => 'success',
        'data' => $result
    );

    if (!$result) {
        $output = array(
            'status' => 204,
            'message' => 'No Data Available',
        );
    }

    Flight::json($output);
});

// Create
Flight::route('POST /peneliti', function () use ($mypdo) {
    $request = Flight::request()->data;
    $nik = $request->nik;
    $nama = $request->nama;
    $telepon = $request->telepon;
    $email = $request->email;
    $pob = $request->pob;
    $dob = $request->dob;
    $sex = $request->sex;
    $pendidikan = $request->pendidikan;
    $instansi = $request->instansi;
    $fakultas = $request->fakultas;
    $jurusan = $request->jurusan;

    if (
        empty($nik) or
        empty($nama) or
        empty($telepon) or
        empty($email)
    ) {
        $output = array(
            'status' => 403,
            'message' => 'failed, data tidak lengkap.'
        );

        Flight::json($output);
        die();
        // Flight::halt($output);
    }

    $query = "INSERT INTO peneliti (
            nik,
            nama_peneliti,
            telepon,
            email,
            pob,
            dob,
            sex,
            pendidikan,
            instansi,
            fakultas,
            jurusan
        )
        VALUES (
            '$nik',
            '$nama',
            '$telepon',
            '$email',
            '$pob',
            '$dob',
            '$sex',
            '$pendidikan',
            '$instansi',
            '$fakultas',
            '$jurusan'
        )";
    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if ($result) {
        $output = array(
            'status' => 201,
            'message' => 'success'
        );
    } else {
        $output = array(
            'status' => 204,
            'message' => 'failed'
        );
    }

    Flight::json($output);
});

// Update
Flight::route('PUT /peneliti/@id', function ($id) use ($mypdo) {
    $request = json_decode(Flight::request()->getBody());
    
    $nik = $request->nik;
    $nama = $request->nama;
    $telepon = $request->telepon;
    $email = $request->email;
    $pob = $request->pob;
    $dob = $request->dob;
    $sex = $request->sex;
    $pendidikan = $request->pendidikan;
    $instansi = $request->instansi;
    $fakultas = $request->fakultas;
    $jurusan = $request->jurusan;

    $query = "UPDATE
            peneliti
        SET
            nik = '$nik',
            nama_peneliti = '$nama',
            telepon = '$telepon',
            email = '$email',
            pob = '$pob',
            dob = '$dob',
            sex = '$sex',
            pendidikan = '$pendidikan',
            instansi = '$instansi',
            fakultas = '$fakultas',
            jurusan = '$jurusan'
        WHERE
            id = $id
    ";

    $stmt = $mypdo->prepare($query);
    $result = $stmt->execute();

    if (!$result) {
        $output = array(
            'status' => 204,
            'message' => 'failed to update data'
        );
    } else {
        $output = array(
            'status' => 201,
            'message' => 'data updated'
        );
    }

    Flight::json($output);
});

// Delete
Flight::route('DELETE /peneliti/@id', function ($id) use ($mypdo) {
    $query = "DELETE FROM peneliti WHERE id=$id";
    $stmt = $mypdo->prepare($query);
    $stmt->execute();

    $output = array(
        'status' => 200,
        'message' => 'success'
    );

    Flight::json($output);
});

Flight::start();
