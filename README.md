# Disclaimer

This api is for education purpose only.

# Requirements

Flight requires `PHP 5.3` or greater.

# License

Flight is released under the [MIT](http://flightphp.com/license) license.
